import QtQuick 2.4
import Ubuntu.Components 1.3
import QtMultimedia 5.6
import Qt.labs.folderlistmodel 2.1


Page {
    id: root_defaultsoundsettingspage

    property bool isDefaultSoundEdited: true
    property alias listview: settings_listview
    property alias audio_play: audio_play
    property string temp_sound

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_defaultsoundsettingspage;
          swipeDistance: swipe_Distance;
          stopAudio: true;
        }

        title: isDefaultSoundEdited ? i18n.tr("Default sound") : i18n.tr("Timer sound")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color;}
        leadingActionBar.actions: Action {
            text: "close"
            iconName: "close"
            // iconSource: Qt.resolvedUrl(backIcon)
            onTriggered: {
                audio_play.stop()
                apl_main.removePages(root_defaultsoundsettingspage)
            }
        }
        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                audio_play.stop()
                isDefaultSoundEdited ? defaultsound = temp_sound : alarmsound = temp_sound
                apl_main.removePages(root_defaultsoundsettingspage)
            }
        }
    }

    Audio {
        id: audio_play

        source: Qt.resolvedUrl(getSoundFile(temp_sound))
        loops: Audio.Infinite
        audioRole: Audio.AlarmRole
    }

    ListItem {
        id: stop_play_item

        width: parent.width
        height: stop_play_button.height + units.gu(2)
        anchors.top: main_header.bottom

        Button {
            id: stop_play_button

            property bool isPlaying: audio_play.playbackState == Audio.PlayingState

            width: Math.min(parent.width - units.gu(4), units.gu(22))
            anchors.centerIn: parent
            text: isPlaying ? i18n.tr("Stop playing") : i18n.tr("Play selected")
            onClicked: isPlaying ? audio_play.stop() : audio_play.play()
        }
    }

    ListView {
        id: settings_listview

        width: parent.width
        height: parent.height - main_header.height - stop_play_item.height
        anchors.top: stop_play_item.bottom
        clip: true

        FolderListModel {
            id: systemSoundModel
            showDirs: true
            nameFilters: [ "*.ogg", "*.mp3" ]
            folder: "/usr/share/sounds/ubports/ringtones"
        }

        ListModel {
          id: combinedSoundModel
          Component.onCompleted: initialise()

          function initialise() {
            combinedSoundModel.append({ "modelData": "sounds/Beeps.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Buzz.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Campanula.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Drrr.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Fanfare.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Harp.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Joy.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Military.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Progressive.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Riff.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Tempus.ogg"})
            combinedSoundModel.append({ "modelData": "sounds/Woods.ogg"})
            for (var i=0; i < systemSoundModel.count; i++) {
              combinedSoundModel.append({ "modelData": systemSoundModel.get(i,"filePath")})
            }
          }
        }

        model: combinedSoundModel

        delegate:
            ListItem {
            id: sound_item

            height: main_layout.height + divider.height
            divider.anchors.leftMargin: units.gu(2)
            divider.anchors.rightMargin: units.gu(2)

            ListItemLayout {
                id: main_layout
                title.text: getFileName(modelData)

                Icon {
                    name: "tick"
                    color: main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: modelData == temp_sound ? 1 : 0
                }
            }
            onClicked: {
                temp_sound = modelData
            }
        }
    }
}
