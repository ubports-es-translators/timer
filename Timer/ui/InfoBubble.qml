import QtQuick 2.4
import Ubuntu.Components 1.3


Rectangle {
    id: root

    color: "#88000000" //8 digit hex color provides transparency with first two digits
    //more info here: https://stackoverflow.com/questions/23201134/transparent-argb-hex-value#28481374

    Rectangle {
        id: bubble

        color: UbuntuColors.porcelain
        width: Math.min(parent.width - units.gu(4), units.gu(50))
        height: Math.min(info_text.implicitHeight + units.gu(4), units.gu(15))
        radius: units.gu(1)
        anchors {
            top: parent.top
            topMargin: parent.height/3
            right: parent.right
            rightMargin: units.gu(2)
        }

        Label {
            id: info_text

            color: UbuntuColors.jet
            width: parent.width - units.gu(4)
            anchors {
                left: parent.left
                leftMargin: units.gu(2)
                top: parent.top
                topMargin: units.gu(2)
            }

            text: i18n.tr("You can add a new timer by pressing the + icon above.")
            wrapMode: Text.WordWrap
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: infoVisible = false
    }

}
