import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_swipedistancepage

    property string temp_dist

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_swipedistancepage;
          swipeDistance: swipe_Distance;
        }

        title: i18n.tr("Swipe distance")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color;}

        leadingActionBar.actions: Action {
          text: "close"
          iconName: "close"
          // iconSource: Qt.resolvedUrl(backIcon)
            onTriggered: {
                apl_main.removePages(root_swipedistancepage)
            }
        }

        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
              swipe_Distance = temp_dist;
              apl_main.removePages(root_swipedistancepage);
            }
        }
    }

    ListView {
        id: settings_listview

        width: parent.width
        height: parent.height - main_header.height
        anchors.top: main_header.bottom
        clip: true

        model: ["5", "10", "15", "20", "25", "30"]

        delegate:
            ListItem {
            id: dist_item

            height: main_layout.height + divider.height

            ListItemLayout {
                id: main_layout

                title.text: modelData + " %"

                Icon {
                    name: "tick"
                    color: main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: modelData === temp_dist ? 1 : 0
                }
            }
            onClicked: {
                temp_dist = modelData;
            }
        }
    }
}
