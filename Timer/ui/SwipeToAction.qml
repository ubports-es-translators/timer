import QtQuick 2.4
import Ubuntu.Components 1.3
import QtMultimedia 5.6

MouseArea {
  id: swipeToActionArea
  anchors.fill: parent
  //properties for SwipetoAction to work and configure
  property double swipeDistance
  property Page apl_page
  property int startPos: 0
  property int diffPos: 0
  //allows click events for items underneath the mouse area
  propagateComposedEvents: true
  //register x position when tapped/clicked
  onPressed: { startPos = mouse.x}

  //needed for landscape view to toogle one/two column mode
  property bool isOneCol//: false

  //needed to stop audio playing when returning form sound setting pages
  property alias audio_play: audio_play
  property bool stopAudio: false
  property var emptyLoader: ""


  onPositionChanged: {
    diffPos = Math.abs(mouse.x - startPos)
    //if moved more than the set distance of the header area to the right or left, trigger the action
    if (diffPos > swipeToActionArea.width * swipeDistance/100) {
      isOneCol ? isOneColumnLayout = true : isOneColumnLayout = false;
      if (emptyLoader == "") {
          apl_main.removePages(apl_page);
      } else {
          //this is only called so far from timer preview page
          emptyLoader.item.setTimer()
          emptyLoader.source = ""
          main_page.header = main_header
      }
      if (stopAudio) { audio_play.stop() }
    }
  }

  Audio {
      id: audio_play
      audioRole: Audio.AlarmRole
  }
}
