import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtSystemInfo 5.0
import "Storage.js" as Storage

Window {
    id: main_window

    property bool isLandscape: main_window.width > main_window.height
    property bool isWide: main_window.width > units.gu(80)
    property bool isHigh: main_window.height > units.gu(66)
    property bool isOneColumnLayout: true
    property bool isDisplayOn: false
    property bool isTimerRunning: false
    property bool isUnlocked: true
    property bool infoVisible: true
    property int lasthvalue: 0 // for restoring latest hour timer setting
    property int lastmvalue: 0 // for restoring latest minute timer setting
    property int lastsvalue: 0 // for restoring latest second timer setting
    property string lasttimername // for restoring latest name of timer
    property var lasttimersound // for restoring latest sound of timer
    property var sinceFinish
    property bool warningVisible: true
    property var defaultsound: "sounds/Drrr.ogg"
    property var alarmsound: defaultsound
//    property string picked_sound: defaultsound
    property string alarm_title
    property string old_alarm_message
    property alias fav_page: fav_page
    property alias main_page: main_page
    property string current_app_Theme: "System"
    property color top_text_color: theme.palette.normal.backgroundSecondaryText
    property color top_back_color: theme.palette.normal.background
    property color main_back_color: theme.palette.normal.background
    property color progress_back_color: theme.palette.normal.backgroundSecondaryText
    property color progress_color: theme.palette.normal.backgroundSecondaryText
    property color headdivider_color: theme.palette.normal.backgroundTertiaryText
    property string current_clock_theme: "Standard"
    property bool swipeDirectionLeft: true
    property bool show_Close_Button: false
    property string swipe_Distance: "15"
    property string backIcon: "swipetoback.svg"
    property var keys01: [Qt.Key_0, Qt.Key_1]
    property var keys29: [Qt.Key_2, Qt.Key_3, Qt.Key_4, Qt.Key_5, Qt.Key_6, Qt.Key_7, Qt.Key_8, Qt.Key_9]
    property var keys05: [Qt.Key_0, Qt.Key_1, Qt.Key_2, Qt.Key_3, Qt.Key_4, Qt.Key_5]
    property var keys69: [Qt.Key_6, Qt.Key_7, Qt.Key_8, Qt.Key_9]
    property var keyERS: [Qt.Key_Enter, Qt.Key_Return, Qt.Key_Space]

    function getFileName(value)
    {
        var path = ""
        if (value != undefined) {
            path = value.toString();
        }
        var lastIndex = path.lastIndexOf("/");
        var file = path.slice(lastIndex + 1);
        var lastIndexPeriod = file.lastIndexOf(".");
        return file.slice(0, lastIndexPeriod);
    }

    function setCurrentTheme() {
        if (current_app_Theme == "System") {
          theme.name = "";
        } else if (current_app_Theme == "Suru-dark") {
          theme.name = "Ubuntu.Components.Themes.SuruDark"
        } else if (current_app_Theme == "Ambiance") {
          theme.name = "Ubuntu.Components.Themes.Ambiance"
        //SuruGradient theme does not look good, that would need quite some recoloring
        //--> therefore I will not make this available, but leave it here for reference
        // } else if (current_app_Theme == "Suru-Gradient") {
        //   theme.name = "Ubuntu.Components.Themes.SuruGradient"
        } else {
          theme.name = "";
        }
    }

    function getSoundFile(value)
    {
        var path = value.toString();
        var lastIndex = path.lastIndexOf("/");
        var file = path.slice(lastIndex + 1);
        if (path.search("/usr/share/sounds/ubports/ringtones") > -1) {
          file = path //return full path for system sound files
        }
        return file
    }

    function unitsdisplay(units)
    {
        if (units > 59){
            units = (units % 60) - 60
        }
        if (units < 0){
            units = (units % 60) + 60
        }
        if (units === 60){
            units = 0
        }

        return ("0"+units).slice(-2)
    }

    function tempEndTime(h, m, s)
    {
        var startTime = Date.now()
        var timeDiff = h * 60 * 60 * 1000 + m * 60 * 1000 + s * 1000
        var endTime = startTime + timeDiff

        return endTime
    }

    function timeLenghtToHMS(time, object)
    {
        var h, m, s
        s = Math.floor(time / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;

        return [
                    object.s = s,
                    object.m = m,
                    object.h = h
                ]
    }

    function timeLenghtToDate(time)
    {
        var hours = ("0" + Math.floor(time / 1000 / 60 / 60)).slice(-2);
        time -= hours*1000*60*60
        var mins = ("0" + Math.floor(time / 1000 / 60)).slice(-2);
        time -= mins*1000*60
        var secs = ("0" + Math.floor(time / 1000)).slice(-2);
        return  hours + ':' + mins + ':' + secs
    }

    function deleteAlarm(alarm_date, alarm_message)
    {
        for (var i = 0; i < alarm_model.count; i++) {
            if (alarm_model.get(i).date.getTime() === alarm_date && alarm_model.get(i).message == alarm_message){
                alarm_model.get(i).cancel()
                alarm_model.get(i).reset()
            }
        }
    }

    function checkIfTimerCreated()
    {
        if(alarm_model.count > 0) { // there are some alarms
            for (var i = 0; i < alarm_model.count; i++){
                if (alarm_model.get(i).message == old_alarm_message){ // there is timer with the same name
                    if (alarm_model.get(i).date < Date.now()){ // timer is finished
                        checkAlarmTimer.stop()
                        apl_main.primaryPage = main_page
                        alarm.date = main_page.main_time_setter.tempendtime = alarm_model.get(i).date
                        alarm.message = alarm_model.get(i).message
                        main_page.main_time_setter.isRunning = isTimerRunning = true
                    }
                    else { // timer is still not finished
                        checkAlarmTimer.stop()
                        if(apl_main.primaryPage !== main_page){
                            apl_main.primaryPage = main_page
                        }
                        alarm.date = main_page.main_time_setter.tempendtime = alarm_model.get(i).date
                        alarm.message = alarm_model.get(i).message
                        main_page.main_time_setter.isRunning = isTimerRunning = true
                    }
                }
                else { // there's no timer with the same name
                    checkAlarmTimer.stop()
                    if (warningVisible) { // show first start warning
                        apl_main.addPageToCurrentColumn(main_page, Qt.resolvedUrl("Warning.qml"))
                    }
                    else { // load main page
                        if(apl_main.primaryPage !== main_page){
                            apl_main.primaryPage = main_page
                        }
                        if(main_page.timers_model.count < 1) {
                            main_page.main_time_setter.time_setter.h = lasthvalue
                            main_page.main_time_setter.time_setter.m = lastmvalue
                            main_page.main_time_setter.time_setter.s = lastsvalue
                            alarm_title = ""
                        }
                    }
                }
            }
        }
        else { // no alarms at all
            checkAlarmTimer.stop()
            if (warningVisible) { // show first start warning
                apl_main.addPageToCurrentColumn(main_page, Qt.resolvedUrl("Warning.qml"))
            }
            else { // load main page
                if(apl_main.primaryPage !== main_page){
                    apl_main.primaryPage = main_page
                }
                if(main_page.timers_model.count < 1) {
                    main_page.main_time_setter.time_setter.h = lasthvalue
                    main_page.main_time_setter.time_setter.m = lastmvalue
                    main_page.main_time_setter.time_setter.s = lastsvalue
                    alarm_title = ""
                    main_page.main_time_setter.timername = lasttimername
                }
            }
        }
    }

    minimumWidth: units.gu(40)
    minimumHeight: units.gu(70)

    Settings {
        property alias warningVisible: main_window.warningVisible
        property alias infoVisible: main_window.infoVisible
        property alias defaultSound: main_window.defaultsound
        property alias displayOn: main_window.isDisplayOn
        property alias currentClockTheme: main_window.current_clock_theme
        property alias oldAlarmTile: main_window.alarm_title
        property alias oldAlarmMessage: main_window.old_alarm_message
        property alias showCloseButton: main_window.show_Close_Button
        property alias appTheme: main_window.current_app_Theme
        property alias lastHValue: main_window.lasthvalue
        property alias lastMValue: main_window.lastmvalue
        property alias lastSValue: main_window.lastsvalue
        property alias lastTimername: main_window.lasttimername
        property alias swipeDirectionLeft: main_window.swipeDirectionLeft
        property alias backIcon: main_window.backIcon
        property alias swipeDistance: main_window.swipe_Distance
    }

    AlarmModel {
        id: alarm_model
        // make sure alarm_model is loaded
        onModelReset: checkAlarmTimer.start()
    }

    Alarm {
        id: alarm
    }

    Timer {
        id: checkAlarmTimer

        interval: 100
        running: false
        repeat: true
        onTriggered: checkIfTimerCreated()
    }


    ScreenSaver {
        id: screen_saver
        screenSaverEnabled: !isDisplayOn || !(Qt.application.state == Qt.ApplicationActive)
    }

    MainView {
        id: main_view
        // Note! applicationName needs to match the "name" field of the click manifest
        applicationName: "timerpro.mivoligo"
        anchors.fill: parent
        backgroundColor: main_back_color

        signal themeChanged()

        Component.onCompleted: {
            if (Storage.checkIfDBVersionTableExists() == "not_exist"){
                console.log("table not exists")
                //create table
                Storage.createDBVersionTable();
                //add value to table
                Storage.saveDBVersion(1);
                // Add column for sounds to task table
                if (Storage.checkIfFavsTableExists() == "table_exist"){
                    Storage.addSoundsColumnToTable()
                }
            }
            Storage.createFavsTable()
            Storage.createTimersTable()

            setCurrentTheme()
        }

        PageStack {
            id: mainViewStack
        }

        onThemeChanged: { setCurrentTheme() }

        AdaptivePageLayout {
            id: apl_main

            anchors.fill: parent
            Component.onCompleted: primaryPage = main_page
            layouts: [
                PageColumnsLayout {
                    when: isWide && !isOneColumnLayout
                    // column #0
                    PageColumn {
                        fillWidth: true
                    }

                    // column #1
                    PageColumn {
                        minimumWidth: units.gu(40)
                        maximumWidth: units.gu(40)
                        preferredWidth: units.gu(40)
                    }
                },
                PageColumnsLayout {
                    when: true
                    PageColumn {
                        fillWidth: true
                        minimumWidth: units.gu(40)
                    }
                }
            ]

            MainPage {
                id: main_page
            }

            FavPage {
                id: fav_page
            }

            EditTimer {
                id: edit_timer_page
            }
        }
    }
}
