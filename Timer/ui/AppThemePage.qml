import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_appthemepage

    property string temp_app_theme

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_appthemepage;
          swipeDistance: swipe_Distance;
        }

        title: i18n.tr("App theme")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color;}

        leadingActionBar.actions: Action {
            text: "close"
            iconName: "close"
            // iconSource: Qt.resolvedUrl(backIcon)
            onTriggered: {
                apl_main.removePages(root_appthemepage)
            }
        }

        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                // current_app_Theme = temp_app_theme
                // main_window.setCurrentTheme();
                apl_main.removePages(root_appthemepage)
            }
        }
    }

    ListView {
        id: settings_listview

        width: parent.width
        height: parent.height - main_header.height
        anchors.top: main_header.bottom
        clip: true

        model: ["System", "Ambiance", "Suru-dark"] //, "Suru-Gradient"

        delegate:
            ListItem {
            id: theme_item

            height: main_layout.height + divider.height

            ListItemLayout {
                id: main_layout

                title.text: modelData

                Icon {
                    name: "tick"
                    color: main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Trailing
                    opacity: modelData == temp_app_theme ? 1 : 0
                }
            }
            onClicked: {
                temp_app_theme = modelData;
                current_app_Theme = modelData;
                main_window.setCurrentTheme();
            }
        }
    }
}
